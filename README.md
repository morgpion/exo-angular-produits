# ExoProduits

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.8.

[Angular documentation](https://angular.io/)

## Angular installation 

*  **Install npm** : `npm install`
*  **Install Angular-cli** : `npm -g @angular/cli`


## Code scaffolding

*  **Start a new project** : `ng new <newproject>`

Move the terminale into the created folder :
`cd <newproject>`

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Or run `ng serve --open` to open de browser from the cli.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

The build may be sent on the server for online publication.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).




