//  import utilitaire pour run le service
import { Injectable } from '@angular/core';

//  import product model
import {Product} from './product.model';

//  import des informations de connexion à la bdd
import {HttpClient} from '@angular/common/http';

//  décorateur qui indique ce qui doit être injecté 
//  comme dépendances dans la classe ProductService (DI)
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private REST_API_SERVER = 'http://localhost:3000/products/';

  constructor(
    private http: HttpClient
  ) { }

  getProducts() {
    return this.http.get<Product[]>(this.REST_API_SERVER);
  }

  getProductById(id: string) {
    return this.http.get<Product>(this.REST_API_SERVER + id);
  }

  createProduct(data: Product) {
    return this.http.post<Product>(this.REST_API_SERVER, data);
  }

  updateProduct(id: string, data: Product) {
    return this.http.put<Product>(this.REST_API_SERVER + id, data);
  }

  deleteProduct(id: string) {
    return this.http.delete<Product>(this.REST_API_SERVER + id);
  }



}
