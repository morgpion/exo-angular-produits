//  import des utilitaires de base
import { Component, OnInit } from '@angular/core';

//  import du module service pour product
import {ProductService} from './product.service';
import {ActivatedRoute, Router} from '@angular/router';

//  ProductAddComponent
//  décorateur pour indiquer à la class ProductComponent les template 
//  et metadata dont elle aura besoin
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  products = [];
  title = 'Nos produits';

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.productService.getProducts().subscribe((products) => {
      console.log(products);
      this.products = products;
    })

  }



}
