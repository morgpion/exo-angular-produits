
import {ProductService} from './../product.service';

import { Component, OnInit } from '@angular/core';
//  import {ActivatedRoute, Router} from '@angular/router';
import { Product } from '../product.model';


@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})

export class ProductAddComponent implements OnInit {

  constructor(
    private productService: ProductService
  ) { }

  product: Product = {
    name: '',
    description: ''
  };
  isSubmit = false;

  addPrdtTitle: "Ajouter un produit";

  ngOnInit(): void {
  }

  //  stocker les données du produit dans le model
  saveProduct() {
    const data = {
      name: this.product.name,
      description: this.product.description
    }
    
    this.productService.createProduct(data).subscribe((result) => {
      this.isSubmit = true;
    }, (error) => {
      console.log(error);
    });
  }

  //  Pour afficher un nouveau form vide si on veut ajouter un autre produit
  newProduct() {
    this.isSubmit = false;
    this.product = {
      name: '',
      description: ''
    }
  }

}
