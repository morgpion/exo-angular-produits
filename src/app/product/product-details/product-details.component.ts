
//  importer le service de Product pour pouvoir l'utiliser
import { ProductService } from './../product.service';
//  import du module Router pour pouvoir l'utliser
import {ActivatedRoute, Router} from '@angular/router';

import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  currentProduct = null;
  titleDetail = "Détails produit";
  //  title = 'Nos produits';

  //  paramètres du constructeur
  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getProduct(this.route.snapshot.paramMap.get('id'));
  }

  getProduct(id) {
    this.productService.getProductById(id).subscribe((product) => {
      this.currentProduct = product;
    }, (error) => {
      console.log(error);
    })
  }

}
