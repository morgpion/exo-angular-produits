
import { Component, OnInit } from '@angular/core';
import {ProductService} from './../product.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Product} from './../product.model';

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})

export class ProductUpdateComponent implements OnInit {

  product = null;
  isSubmit = false;
  updateTitle = "Modifier un produit";

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getProductById(this.route.snapshot.paramMap.get('id'));
  }

  getProductById(id) {
    this.productService.getProductById(id).subscribe((product) => {
      this.product = product;
    }, (error) => {
      console.log(error);
    })
  }

  updateProduct() {
    const data = {
      name: this.product.name,
      description: this.product.description
    };
    
    const id = this.product._id;
    
    this.productService.updateProduct(id, data).subscribe(() => {
      this.isSubmit = true;
    }, (err) => {
      console.log(err);
    })
  }



}


