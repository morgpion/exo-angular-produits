
import {ProductUpdateComponent} from './product/product-update/product-update.component';
import {ProductDeleteComponent} from './product/product-delete/product-delete.component';
import { ProductAddComponent } from './product/product-add/product-add.component';
import {ProductDetailsComponent} from './product/product-details/product-details.component';
import {ProductComponent} from './product/product.component';

import { NgModule } from '@angular/core'; //  , Component
import { Routes, RouterModule } from '@angular/router';
//  import { ProductDeleteComponent } from './product/product-delete/product-delete.component';



const routes: Routes = [
  {path: '', redirectTo: 'products', pathMatch: 'full'},
  {path: 'product/:id', component: ProductDetailsComponent},
  {path: 'updateProduct/:id', component: ProductUpdateComponent},
  {path: 'delProduct/:id', component: ProductDeleteComponent},
  {path: 'products/add', component: ProductAddComponent},
  {path: 'products', component: ProductComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

