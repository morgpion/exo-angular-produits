//  import utilitaire de component
import { Component } from '@angular/core';

//  décorateur qui indique à la classe AppComponent les templates 
//  et métadatas pour l'affichage des pages
//  on exporte la classe pour pouvoir y accéder depuis les modules
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'exo produits';
}
